package com.digdes.school;

import com.digdes.school.view.GeneralView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JavaSchoolStarter {
    public JavaSchoolStarter(){

    }
    private final GeneralView generalView = new GeneralView();

    public List<Map<String,Object>> execute(String request) throws Exception {
          return generalView.doAction(request);

    }

}
