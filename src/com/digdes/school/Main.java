package com.digdes.school;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        JavaSchoolStarter starter = new JavaSchoolStarter();

        try {
            List<Map<String,Object>> result1 = starter.execute("INSERT VALUES 'lastName' = 'Федоров' , 'id'=3, 'age'=10, 'active'=false");
            List<Map<String,Object>> result5 = starter.execute("INSERT VALUES 'lastName' = 'Пупа Лупа' , 'id'=65, 'age'=40, 'active'=true, 'cost'=3.5");
            List<Map<String,Object>> result49 = starter.execute("INSERT VALUES 'lastName' = 'Тарантино' , 'id'=66, 'age'=30, 'active'=true, 'cost'=3.2");
            List<Map<String,Object>> result50 = starter.execute("INSERT VALUES 'lastName' = 'Тарантино Иванович' , 'id'=67, 'age'=40, 'active'=true, 'cost'=3.2");
            List<Map<String,Object>> result51 = starter.execute("INSERT VALUES 'lastName' = 'Квентин Тарантино' , 'id'=68, 'age'=12, 'active'=false, 'cost'=3.2");
            List<Map<String,Object>> result52 = starter.execute("INSERT VALUES 'lastName' = 'Квентин Тарантино Иванович' , 'id'=69, 'age'=12, 'active'=true, 'cost'=3.6");
            List<Map<String,Object>> result7 = starter.execute("SELECT WHERE 'lastName'!='Тарантино' AND 'cost'>=3.2");
            List<Map<String,Object>> result71 = starter.execute("SELECT WHERE 'lastName'='Тарантино' OR 'cost'< 3.6");
//            List<Map<String,Object>> result6 = starter.execute("SELECT");
//            List<Map<String,Object>> result8 = starter.execute("SELECT WHERE 'age'=10");
            List<Map<String,Object>> result9 = starter.execute("SELECT WHERE 'lastName' LIKE '%ТарантИно' OR 'cost'>3.2");
//            List<Map<String,Object>> result10 = starter.execute("UPDATE VALUES 'lastName' = null, 'id'=999 where 'age' = 12 or 'active'=false");
//            List<Map<String,Object>> result11 = starter.execute("UPDATE VALUES 'lastName' = 'new name', 'cost'=5.5 where 'age' < 13");
            List<Map<String,Object>> result12 = starter.execute("UPDATE VALUES 'age' = 10 where 'id' < 67");
            List<Map<String,Object>> result13 = starter.execute("DELETE where 'age' < 13");
            result9.forEach(System.out::println);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        

    }
}
