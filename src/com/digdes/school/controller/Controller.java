package com.digdes.school.controller;

import com.digdes.school.filter.Filter;
import com.digdes.school.service.Service;

import java.util.*;

public class Controller {

    Map<String, Object> map;
    List<Map<String, Object>> result = new ArrayList<>();

    private final Service service = new Service();

    public List<Map<String, Object>> select(String command) {
        String selectWithoutArguments = command.trim();
        if (selectWithoutArguments.length() == 6) {
            return result;
        } else {
            String lowerCaseCommand = command.toLowerCase(Locale.ROOT);
            if (isConditionExist(lowerCaseCommand)) {
                int beginIndex = lowerCaseCommand.indexOf("where") + 5;
                return service.selectByCondition(command, beginIndex, result);
            } else {
                throw new IllegalArgumentException("Нет оператора where");
            }
        }
    }


    public List<Map<String, Object>> insert(String command) {
        fillMap();
        String methodName = command.substring(0,13);
        String substring = command.substring(14);
        String[] arguments = substring.split(",");
        for (String argument : arguments) {
            String trim = argument.trim();
            try {
               service.putValueToMap(trim, map, methodName);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if (map.values().stream().allMatch(Objects::isNull)) {
            throw new IllegalArgumentException("Все значения null");
        }
        result.add(map);
        return result;
    }

    public List<Map<String, Object>> update(String request) {
        String methodName = request.substring(0,13);
        String command = request.substring(14);
        String lowerCaseCommand = command.toLowerCase(Locale.ROOT);
        String[] arguments = command.split(",");
        if (!isConditionExist(lowerCaseCommand)){
            updateValues(methodName, arguments, result);
        } else  {
            int beginIndexOfCondition = lowerCaseCommand.indexOf("where") + 5;
            int endIndexOfArguments = lowerCaseCommand.indexOf("where");
            arguments = command.substring(0, endIndexOfArguments).split(",");
            List<Map<String, Object>> meetTheConditionList = service.selectByCondition(command, beginIndexOfCondition, result);
            updateValues(methodName, arguments, meetTheConditionList);
        }
        return result;
    }

    public List<Map<String, Object>> delete(String request) {
        String deleteWithoutArguments = request.trim();
        if (deleteWithoutArguments.length() == 6) {
            return result = new ArrayList<>();
        } else {
            String command = request.substring(7);
            String lowerCaseCommand = command.toLowerCase(Locale.ROOT);
            if (isConditionExist(lowerCaseCommand)){
                int beginIndexOfCondition = lowerCaseCommand.indexOf("where") + 5;
                List<Map<String, Object>> meetTheConditionList = service.selectByCondition(command, beginIndexOfCondition, result);
                result.removeAll(meetTheConditionList);
                return result;
            } else {
                throw new IllegalArgumentException("Нет оператора where");
            }
        }
    }

    private void updateValues(String methodName, String[] arguments, List<Map<String, Object>> list) {
        try {
            for (Map<String, Object> x : list) {
                for (String argument : arguments) {
                    String trim = argument.trim();
                    service.putValueToMap(trim, x, methodName);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isConditionExist(String substring) {
        String[] split = substring.split(" ");
        return Arrays.stream(split).anyMatch(s -> s.equals("where"));
    }

    private void fillMap() {
        map = new HashMap<>();
        map.put("id", null);
        map.put("lastName", null);
        map.put("age", null);
        map.put("cost", null);
        map.put("active", null);
    }
}
