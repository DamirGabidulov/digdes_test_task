package com.digdes.school.filter;

import java.util.*;

public class Filter {

    private static final String EQUAL = "=";
    private static final String NOT_EQUAL = "!=";
    private static final String MORE_OR_EQUAL = ">=";
    private static final String LESS_OR_EQUAL = "<=";
    private static final String MORE = ">";
    private static final String LESS = "<";
    private static final String LIKE = "like";
    private static final String ILIKE = "ilike";

    public Map<String, String> identifyOperator(Object o, int endIndex) {
        Map<String, String> operatorAndValue = new HashMap<>();
        switch (o) {
            case String s && s.substring(endIndex).trim().startsWith(EQUAL) -> {
                int indexOf = s.indexOf(EQUAL);
                String value = s.substring(indexOf + 1).trim();
                operatorAndValue.put(EQUAL, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().startsWith(NOT_EQUAL) -> {
                int indexOf = s.indexOf(NOT_EQUAL);
                String value = s.substring(indexOf + 2).trim();
                operatorAndValue.put(NOT_EQUAL, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().startsWith(MORE_OR_EQUAL) -> {
                int indexOf = s.indexOf(MORE_OR_EQUAL);
                String value = s.substring(indexOf + 2).trim();
                operatorAndValue.put(MORE_OR_EQUAL, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().startsWith(LESS_OR_EQUAL) -> {
                int indexOf = s.indexOf(LESS_OR_EQUAL);
                String value = s.substring(indexOf + 2).trim();
                operatorAndValue.put(LESS_OR_EQUAL, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().startsWith(MORE) -> {
                int indexOf = s.indexOf(MORE);
                String value = s.substring(indexOf + 1).trim();
                operatorAndValue.put(MORE, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().startsWith(LESS) -> {
                int indexOf = s.indexOf(LESS);
                String value = s.substring(indexOf + 1).trim();
                operatorAndValue.put(LESS, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().toLowerCase(Locale.ROOT).startsWith(LIKE) -> {
                int indexOf = s.toLowerCase(Locale.ROOT).indexOf(LIKE);
                String value = s.substring(indexOf + 4).trim();
                operatorAndValue.put(LIKE, value);
                return operatorAndValue;
            }
            case String s && s.substring(endIndex).trim().toLowerCase(Locale.ROOT).startsWith(ILIKE) -> {
                int indexOf = s.toLowerCase(Locale.ROOT).indexOf(ILIKE);
                String value = s.substring(indexOf + 5).trim();
                operatorAndValue.put(ILIKE, value);
                return operatorAndValue;
            }

            default -> throw new IllegalStateException("��� ���������");
        }
    }

    public List<Map<String, Object>> filterByArguments(String argument, Object o, Object value, List<Map<String, Object>> list) {
        switch (o) {
            case String s && s.equals(EQUAL) -> {
                return list.stream()
                        .filter(x -> x.get(argument) != null)
                        .filter(x -> x.get(argument).equals(value))
                        .toList();
            }
            case String s && s.equals(NOT_EQUAL) -> {
                List<Map<String, Object>> argumentsWithNullValue = list.stream()
                        .filter(x -> x.get(argument) == null).toList();

                List<Map<String, Object>> argumentsWithExistedValue = list.stream()
                        .filter(x -> x.get(argument) != null)
                        .filter(x -> !x.get(argument).equals(value))
                        .toList();
                //��������� null � �������� �������� ��� ��� �� ������� ������ ������ 0!=null ��������� ����������
                Set<Map<String, Object>> mapSet = new HashSet<>();
                mapSet.addAll(argumentsWithNullValue);
                mapSet.addAll(argumentsWithExistedValue);
                return new ArrayList<>(mapSet);
            }
            case String s && s.equals(MORE_OR_EQUAL) -> {
                if (argument.equals("id") || argument.equals("age")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Long) x.get(argument) >= (Long) value)
                            .toList();
                } else if (argument.equals("cost")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Double) x.get(argument) >= (Double) value)
                            .toList();
                } else {
                    throw new IllegalStateException("������������ ������ �������� " + argument);
                }
            }
            case String s && s.equals(LESS_OR_EQUAL) -> {
                if (argument.equals("id") || argument.equals("age")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Long) x.get(argument) <= (Long) value)
                            .toList();
                } else if (argument.equals("cost")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Double) x.get(argument) <= (Double) value)
                            .toList();
                } else {
                    throw new IllegalStateException("������������ ������ �������� " + argument);
                }
            }
            case String s && s.equals(MORE) -> {
                if (argument.equals("id") || argument.equals("age")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Long) x.get(argument) > (Long) value)
                            .toList();
                } else if (argument.equals("cost")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Double) x.get(argument) > (Double) value)
                            .toList();
                } else {
                    throw new IllegalStateException("������������ ������ �������� " + argument);
                }
            }
            case String s && s.equals(LESS) -> {
                if (argument.equals("id") || argument.equals("age")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Long) x.get(argument) < (Long) value)
                            .toList();
                } else if (argument.equals("cost")) {
                    return list.stream()
                            .filter(x -> x.get(argument) != null)
                            .filter(x -> (Double) x.get(argument) < (Double) value)
                            .toList();
                } else {
                    throw new IllegalStateException("������������ ������ �������� " + argument);
                }
            }
            case String s && s.equals(LIKE) -> {
                if (argument.equals("lastName")) {
                    if (value.toString().startsWith("%") && value.toString().endsWith("%")) {
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().contains(value.toString().substring(1, value.toString().length() - 1)))
                                .toList();
                    }else if (value.toString().startsWith("%")){
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().endsWith(value.toString().substring(1)))
                                .toList();
                    } else if (value.toString().endsWith("%")) {
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().startsWith(value.toString().substring(0, value.toString().length() - 1)))
                                .toList();
                    }else {
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).equals(value))
                                .toList();
                    }
                } else {
                    throw new IllegalStateException("������������ ������ �������� " + argument);
                }
            }
            case String s && s.equals(ILIKE) -> {
                if (argument.equals("lastName")) {
                    String loverCaseValue = value.toString().toLowerCase(Locale.ROOT);
                    if (value.toString().startsWith("%") && value.toString().endsWith("%")) {
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().toLowerCase(Locale.ROOT).contains(loverCaseValue.substring(1, loverCaseValue.length() - 1)))
                                .toList();
                    }else if (value.toString().startsWith("%")){
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().toLowerCase(Locale.ROOT).endsWith(loverCaseValue.substring(1)))
                                .toList();
                    } else if (value.toString().endsWith("%")) {
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().toLowerCase(Locale.ROOT).startsWith(loverCaseValue.substring(0, loverCaseValue.length() - 1)))
                                .toList();
                    }else {
                        return list.stream()
                                .filter(x -> x.get(argument) != null)
                                .filter(x -> x.get(argument).toString().toLowerCase(Locale.ROOT).equals(loverCaseValue))
                                .toList();
                    }
                } else {
                    throw new IllegalStateException("������������ ������ �������� " + argument);
                }
            }


            default -> throw new IllegalStateException("��� ���������");
        }
    }
}
