package com.digdes.school.service;

import com.digdes.school.filter.Filter;

import java.util.*;

public class Service {
    private final Filter filter = new Filter();

    public List<Map<String, Object>> selectByCondition(String command, int beginIndex, List<Map<String, Object>> result) {

        String condition = command.substring(beginIndex).trim();
        String[] split = condition.toLowerCase(Locale.ROOT).split(" ");
        if (Arrays.stream(split).anyMatch(x -> x.equals("and"))) {
            int indexOfOperator = condition.toLowerCase(Locale.ROOT).indexOf("and");
            String firstPartOfCondition = condition.substring(0, indexOfOperator).trim();
            String secondPartOfCondition = condition.substring(indexOfOperator + 3).trim();
            try {
                List<Map<String, Object>> firstPart = selectByArguments(firstPartOfCondition, result);
                List<Map<String, Object>> secondPart = selectByArguments(secondPartOfCondition, firstPart);
                return secondPart;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if (Arrays.stream(split).anyMatch(x -> x.equals("or"))) {
            int indexOfOperator = condition.toLowerCase(Locale.ROOT).indexOf("or");
            String firstPartOfCondition = condition.substring(0, indexOfOperator).trim();
            String secondPartOfCondition = condition.substring(indexOfOperator + 2).trim();
            try {
                List<Map<String, Object>> firstPart = selectByArguments(firstPartOfCondition, result);
                List<Map<String, Object>> secondPart = selectByArguments(secondPartOfCondition, result);
                Set<Map<String, Object>> mapSet = new HashSet<>();
                mapSet.addAll(firstPart);
                mapSet.addAll(secondPart);
                List<Map<String, Object>> listOfResults = new ArrayList<>(mapSet);
                return listOfResults;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                List<Map<String, Object>> listOfResults = selectByArguments(condition, result);
                return listOfResults;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void putValueToMap(Object o, Map<String, Object> map, String methodName) throws Exception {
        switch (o) {
            case String s && s.toLowerCase().startsWith("'lastname'") -> {
                int indexOf = s.indexOf("=");
                String value = s.substring(indexOf + 1).replace("'", "").trim();
                if (isMethodUpdate(map, methodName, s, indexOf, "lastName")) break;
                if (value.matches("\\d+") || value.matches("true") || value.matches("false")) {
                    throw new Exception("������������ ������ �������� �����");
                }
                map.put("lastName", value);
            }
            case String s && s.toLowerCase().startsWith("'id'") -> {
                int indexOf = s.indexOf("=");
                if (isMethodUpdate(map, methodName, s, indexOf, "id")) break;
                try {
                    Long value = Long.valueOf(s.substring(indexOf + 1).trim());
                    map.put("id", value);
                } catch (NumberFormatException e) {
                    throw new Exception("������������ ������ �������� id");
                }
            }
            case String s && s.toLowerCase().startsWith("'age'") -> {
                int indexOf = s.indexOf("=");
                if (isMethodUpdate(map, methodName, s, indexOf, "age")) break;
                try {
                    Long value = Long.valueOf(s.substring(indexOf + 1).trim());
                    map.put("age", value);
                } catch (NumberFormatException e) {
                    throw new Exception("������������ ������ �������� age");
                }
            }
            case String s && s.toLowerCase().startsWith("'cost'") -> {
                int indexOf = s.indexOf("=");
                if (isMethodUpdate(map, methodName, s, indexOf, "cost")) break;
                try {
                    Double value = Double.valueOf(s.substring(indexOf + 1).trim());
                    map.put("cost", value);
                } catch (NumberFormatException e) {
                    throw new Exception("������������ ������ �������� cost");
                }
            }
            case String s && s.toLowerCase().startsWith("'active'") -> {
                int indexOf = s.indexOf("=");
                String subString = s.substring(indexOf + 1).trim();
                if (isMethodUpdate(map, methodName, s, indexOf, "active")) break;
                if (!subString.matches("true") & !subString.matches("false")) {
                    throw new Exception("������������ ������ �������� active");
                }
                Boolean value = Boolean.valueOf(subString);
                map.put("active", value);
            }
            default -> {
                throw new Exception("� ������� ���� ������� � ������������ ���������");
            }
        }
    }

    private List<Map<String, Object>> selectByArguments(Object o, List<Map<String, Object>> list) throws Exception {
        String operator = null;
        String value = null;
        switch (o) {
            case String s && s.toLowerCase().startsWith("'lastname'") -> {
                int endIndex = 10;
                Map<String, String> data = filter.identifyOperator(s, endIndex);
                for (Map.Entry<String, String> x : data.entrySet()) {
                    operator = x.getKey();
                    value = x.getValue();
                }
                if (value.matches("\\d+") || value.matches("true") || value.matches("false")) {
                    throw new Exception("������������ ������ �������� �����");
                }
                String finalValue = value.replace("'", "");
                String argument = "lastName";
                return filter.filterByArguments(argument, operator, finalValue, list);
            }
            case String s && s.toLowerCase().startsWith("'id'") -> {
                int endIndex = 4;
                Map<String, String> data = filter.identifyOperator(s, endIndex);
                for (Map.Entry<String, String> x : data.entrySet()) {
                    operator = x.getKey();
                    value = x.getValue();
                }
                try {
                    Long finalValue = Long.valueOf(value);
                    String argument = "id";
                    return filter.filterByArguments(argument, operator, finalValue, list);
                } catch (NumberFormatException e) {
                    throw new Exception("������������ ������ �������� id");
                }
            }
            case String s && s.toLowerCase().startsWith("'age'") -> {
                int endIndex = 5;
                Map<String, String> data = filter.identifyOperator(s, endIndex);
                for (Map.Entry<String, String> x : data.entrySet()) {
                    operator = x.getKey();
                    value = x.getValue();
                }
                try {
                    Long finalValue = Long.valueOf(value);
                    String argument = "age";
                    return filter.filterByArguments(argument, operator, finalValue, list);
                } catch (NumberFormatException e) {
                    throw new Exception("������������ ������ �������� age");
                }
            }
            case String s && s.toLowerCase().startsWith("'cost'") -> {
                int endIndex = 6;
                Map<String, String> data = filter.identifyOperator(s, endIndex);
                for (Map.Entry<String, String> x : data.entrySet()) {
                    operator = x.getKey();
                    value = x.getValue();
                }
                try {
                    Double finalValue = Double.valueOf(value);
                    String argument = "cost";
                    return filter.filterByArguments(argument, operator, finalValue, list);
                } catch (NumberFormatException e) {
                    throw new Exception("������������ ������ �������� cost");
                }
            }
            case String s && s.toLowerCase().startsWith("'active'") -> {
                int endIndex = 8;
                Map<String, String> data = filter.identifyOperator(s, endIndex);
                for (Map.Entry<String, String> x : data.entrySet()) {
                    operator = x.getKey();
                    value = x.getValue();
                }
                if (!value.matches("true") & !value.matches("false")) {
                    throw new Exception("������������ ������ �������� active");
                }
                Boolean finalValue = Boolean.valueOf(value);
                String argument = "active";
                return filter.filterByArguments(argument, operator, finalValue, list);
            }
            default -> {
                throw new Exception("� ������� ���� ������� � ������������ ���������");
            }
        }
    }

    private boolean isMethodUpdate(Map<String, Object> map, String methodName, String s, int indexOf, String argument) {
        if (methodName.toLowerCase(Locale.ROOT).equals("update values") && s.substring(indexOf + 1).trim().equals("null")){
            map.put(argument, null);
            return true;
        }
        return false;
    }
}
