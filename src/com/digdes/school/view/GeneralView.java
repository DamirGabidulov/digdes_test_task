package com.digdes.school.view;

import com.digdes.school.controller.Controller;

import java.util.List;
import java.util.Map;

public class GeneralView {
    private final Controller controller = new Controller();

    public List<Map<String,Object>> doAction(String request) {
        String command = request.toLowerCase();
        if (command.startsWith("insert values")) {
           return controller.insert(request);
        } else if (command.startsWith("select")) {
           return controller.select(request);
        } else if (command.startsWith("update values")) {
           return controller.update(request);
        } else if (command.startsWith("delete")) {
            return controller.delete(request);
        }
        return null;
    }
}
